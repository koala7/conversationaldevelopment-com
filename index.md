---
layout: home
---
# Agile needs to evolve

We need to stop thinking in terms of story points or columns and focus on the 
conversations in the software development life cycle instead, the time they 
take and the value they bring - making the shift from an abstract to a result 
orientated culture.

Conversational Development is the next step in the evolution Agile, 
[Find out why](/why) your team or organization should start using it. 

Conversational Development is a living, breathing methodology that you can 
improve. [Find out here](/contribute).
