---
---
## Principle 4: Result oriented conversations

Stop measuring abstract things like story points or irrelevant things like 
hours. Instead pro-actively look for ways to connect your work to business 
metrics.

<img style="width:100%" src="/images/R0.png"/>

### Howto

- Instead of hours(in Waterfall) or story points(in Scrum) measuring, start 
  measuring whole conversation cycle time(from idea to production)
- Move towards measuring impact on your business metrics: think of ARR, SALs, 
  MQLs, page views, NPS, ARPU, etc.
- Results should become the most important in performance evaluations.
- If you're Remote Only company, you can't compete on hours, so you are 
  naturally forced to focus on another metric.

*[ARR]: Annual run rate
*[SALs]: Sales accepted lead
*[MQLs]: Marketing qualified lead
*[NPS]: Net promoter score
*[ARPU]: Average revenue per user

Basically, you get what you measure. The more you measure results, the more 
results you get.

<img style="width:100%" src="/images/R1.png" />

[Maturity Model](/maturity-model)
